package net.gagarinn.finalproject.fragments;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.gagarinn.finalproject.R;
import net.gagarinn.finalproject.fragments.RestorantsFragment.OnListFragmentInteractionListener;
import net.gagarinn.finalproject.model.Restorant;

import java.util.List;

public class RestorantItemRecyclerViewAdapter extends RecyclerView.Adapter<RestorantItemRecyclerViewAdapter.ViewHolder> {

    private  List<Restorant> mValues;
    private final OnListFragmentInteractionListener mListener;


    public RestorantItemRecyclerViewAdapter(List<Restorant> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_restorantitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.tvName.setText(mValues.get(position).getName() + "\n" + (mValues.get(position).getRating()) + "\n" +(mValues.get(position).getDistance() + " m"));
//        holder.tvDistanc.setText(mValues.get(position).getDistance());
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public final View mView;
        public final TextView tvName;
//        public final TextView tvDistanc;
        public Restorant mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvName = (TextView) view.findViewById(R.id.tv_name);
//            tvDistanc = (TextView) view.findViewById(R.id.tv_distance);
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.onListFragmentInteraction(mItem);
                    }
                }
            });
        }

       @Override
        public void onClick(View view) {
            // TODO: 31.01.2018  почему этот не работает?
            if (mListener != null) {
                mListener.onListFragmentInteraction(mItem);
            }
        }
    }

    public List<Restorant> getmValues() {
        return mValues;
    }

    public void setmValues(List<Restorant> mValues) {
        this.mValues = mValues;
    }
}
