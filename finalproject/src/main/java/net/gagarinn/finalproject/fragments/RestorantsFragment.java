package net.gagarinn.finalproject.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import net.gagarinn.finalproject.R;
import net.gagarinn.finalproject.controller.RetrofitController;
import net.gagarinn.finalproject.model.Restorant;

import java.util.List;

public class RestorantsFragment extends BaseFragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener listListener;

    RecyclerView recyclerView;
    RestorantItemRecyclerViewAdapter adapter;

    public RestorantsFragment() {
    }

    // TODO: Customize parameter initialization

    public static RestorantsFragment newInstance(int columnCount) {
        RestorantsFragment fragment = new RestorantsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restorantitem_list, container, false);
        listListener = new OnListFragmentInteractionListener() {
            @Override
            public void onListFragmentInteraction(Restorant item) {
                routeToDetailsActivity(item);
            }
        };


        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            adapter = new RestorantItemRecyclerViewAdapter(RetrofitController.getLastVenues(), listListener);
            recyclerView.setAdapter(adapter);
        }
        return view;
    }

    @Override
    protected void onDataRecived(List<Restorant> venues) {
        if (adapter != null) {
            adapter.setmValues(venues);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void showDownloadingEnd() {

    }

    @Override
    protected void showDownloadingBegin() {

    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Restorant item);
    }
}
