package net.gagarinn.finalproject.fragments;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;
import com.google.android.gms.maps.model.LatLng;

import net.gagarinn.finalproject.activities.ResorantDetailsActivity;
import net.gagarinn.finalproject.controller.RetrofitController;
import net.gagarinn.finalproject.model.Constants;
import net.gagarinn.finalproject.model.Restorant;

import java.lang.ref.WeakReference;
import java.util.List;

public abstract class BaseFragment extends Fragment {

    protected OnFragmentInteractionListener fragmentActionsListener;
    protected WeakReference<RetrofitController.OnDataRecivedListener> onDataRecivedListenerRef;
    protected final String TAG = this.getClass().getSimpleName();

    protected static int radius;
    protected static float zoom;
    protected static double lat;
    protected static double lon;


    protected abstract void onDataRecived(List<Restorant> venues);
    protected abstract void showDownloadingEnd();
    protected abstract void showDownloadingBegin();

    public BaseFragment() {
        // Required empty public constructor
    }

    public void updateCurrentAdress(LatLng center) {
        if (center != null) {
            getRestorants(center.latitude, center.longitude);
        }
    }
    protected void getRestorants(final double currentLatitude, final double currentLon) {
        lat = currentLatitude;
        lon = currentLon;
        showDownloadingBegin();
        onDataRecivedListenerRef =  new WeakReference<RetrofitController.OnDataRecivedListener>(new RetrofitController.OnDataRecivedListener() {
            @Override
            public void onPointsRecived(List<Restorant> venues) {
                onDataRecived(venues);
                showDownloadingEnd();
            }
        });

        if (zoom < Constants.MIN_ZOOM ){
            radius = Constants.MAX_RADIUS_METERS;
        }else if(zoom > Constants.MAX_ZOOM){
            radius = Constants.MIN_RADIUS_METERS;
        }
        Log.d(TAG, "------BaseFragment : getRestorants: lat = " + lat);
        Log.d(TAG, "------BaseFragment : getRestorants: lon = " + lon);
        Log.d(TAG, "------BaseFragment : getRestorants: radius = " + radius);
        Log.d(TAG, "------BaseFragment : getRestorants: zoom = " + zoom);
        RetrofitController.getRestorantsSorted(lat, lon, radius, onDataRecivedListenerRef.get());

    }

    protected void routeToDetailsActivity(Restorant restorant){
        if (restorant != null) {
            Intent intent = new Intent(getActivity(), ResorantDetailsActivity.class);
            intent.putExtra(Constants.SELECTED_RESTORANT, restorant);
            startActivity(intent);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            fragmentActionsListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement fragmentActionsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentActionsListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(fragmentActionsListener != null){
            fragmentActionsListener.onFragmentResumed();
        }
    }

    public interface OnFragmentInteractionListener {
        void onUseCarrentLocationTapped();
        void onFragmentResumed();
    }


}
