package net.gagarinn.finalproject.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import net.gagarinn.finalproject.R;
import net.gagarinn.finalproject.controller.RetrofitController;
import net.gagarinn.finalproject.model.Constants;
import net.gagarinn.finalproject.model.Restorant;
import net.gagarinn.finalproject.tools.PermissionUtils;
import net.gagarinn.finalproject.tools.Tools;

import java.util.List;

/**
 * Created by Burmaka V on 22.01.2018.
 */

public class MapFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnCameraMoveListener, GoogleMap.OnInfoWindowClickListener {

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private boolean mPermissionDenied = false;


    ImageButton ibtnUseCurrentLogation;
    ProgressBar pbUseCurrentLogation;

    CircleOptions singleCircle;

    private GoogleMap mMap;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_fragment_map, container, false);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ibtnUseCurrentLogation = (ImageButton)view.findViewById(R.id.ibtn_use_current_location);
        pbUseCurrentLogation = (ProgressBar) view.findViewById(R.id.pb_use_current_location);


        singleCircle = new CircleOptions()
                .strokeWidth(2.0f)
                .strokeColor(Color.BLACK);
        if (zoom < Constants.MIN_ZOOM ){
            zoom = Constants.MIN_ZOOM;
        }

        ibtnUseCurrentLogation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ibtnUseCurrentLogation.setVisibility(View.GONE);
                pbUseCurrentLogation.setVisibility(View.VISIBLE);
                if(fragmentActionsListener != null){
                    fragmentActionsListener.onUseCarrentLocationTapped();
                }

                Location myLocation = null;
                try {
                    myLocation = mMap.getMyLocation(); // TODO: 31.01.2018 fix bug
                }catch (Exception e){
                    Log.e("TODEL", "------ MapFragment.onClick: = " + e.getMessage());
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (myLocation != null) {
                    getRestorants(myLocation.getLatitude(), myLocation.getLongitude());
                }

            }
        });

        return view;
    }

    void setCurrentZoomAndRadius(){
        if (mMap == null){
            return;
        }
        LatLng farLeft = mMap.getProjection().getVisibleRegion().farLeft;
        LatLng center = mMap.getCameraPosition().target;

        float currentZoom = mMap.getCameraPosition().zoom;
        if (currentZoom < Constants.MIN_ZOOM ){
            radius = Constants.MAX_RADIUS_METERS;
            zoom = Constants.MIN_ZOOM;
        }else if(currentZoom > Constants.MAX_ZOOM){
            radius = Constants.MIN_RADIUS_METERS;
            zoom = Constants.MAX_ZOOM;
        }else {
            radius = (int)toRadiusMeters(center, farLeft);
            zoom = currentZoom;
        }
    }

    @Override
    protected void getRestorants(double currentLatitude, double currentLon) {
        setCurrentZoomAndRadius();
        super.getRestorants(currentLatitude, currentLon);
    }


    @Override
    protected void onDataRecived(List<Restorant> venues) {
        setSearchArea(venues);
    }

    @Override
    protected void showDownloadingEnd() {
        ibtnUseCurrentLogation.setVisibility(View.VISIBLE);
        pbUseCurrentLogation.setVisibility(View.GONE);
    }

    @Override
    protected void showDownloadingBegin() {
        ibtnUseCurrentLogation.setVisibility(View.GONE);
        pbUseCurrentLogation.setVisibility(View.VISIBLE);

    }

    @MainThread
    private void setSearchArea(List<Restorant> points) {
        if (points == null){
            return;
        }
        mMap.clear();

        addCircle();



        StringBuilder title = new StringBuilder();
        if (!Tools.isNullOrEmpty(points)){
            if (lat == 0 && lon == 0){
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(points.get(0).getLat(), points.get(0).getLng()), zoom));
            }else {
                mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).title("").icon(BitmapDescriptorFactory.defaultMarker(59.0f)));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), zoom));
            }

            for (Restorant item : points) {
                title.setLength(0);
                title.append(item.getName());
                BitmapDescriptor markerColor = BitmapDescriptorFactory.defaultMarker(159.0f);
                if (item.getRating() > 9){
                    markerColor = BitmapDescriptorFactory.defaultMarker(12.0f);
                }
               Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(item.getLat(), item.getLng())).title(title.toString()).snippet(item.getDistance() + "m , " + item.getRating()).icon(markerColor));
               marker.setTag(item);
            }
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        if (marker.getTag() == null){
            return;
        }
        if ((marker.getTag() instanceof Restorant)) {
            routeToDetailsActivity((Restorant)marker.getTag());
        }
    }

    private static double toRadiusMeters(LatLng center, LatLng radius) {
        float[] result = new float[1];
        if (center != null && radius != null) {
            Location.distanceBetween(center.latitude, center.longitude,
                    radius.latitude, radius.longitude, result);
        }
        return result[0];
    }

    private void addCircle(){
        mMap.addCircle(singleCircle
                .center(new LatLng(lat, lon))
                .radius(radius)
        );
    }


    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;

        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnCameraMoveListener(this);
        mMap.setOnInfoWindowClickListener(this);
        enableMyLocation();

            setSearchArea(RetrofitController.getLastVenues());
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission((AppCompatActivity)getActivity(), LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if  (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getChildFragmentManager(), "dialog");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            enableMyLocation();
        } else {
            mPermissionDenied = true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPermissionDenied) {
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    @Override
    public void onCameraMove() {

    }
}

