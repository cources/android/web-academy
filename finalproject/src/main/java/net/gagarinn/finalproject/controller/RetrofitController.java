package net.gagarinn.finalproject.controller;

import android.content.Context;
import android.util.Log;

import net.gagarinn.finalproject.db.DBHelper;
import net.gagarinn.finalproject.model.Constants;
import net.gagarinn.finalproject.model.Restorant;
import net.gagarinn.finalproject.model.explore.FourSquareRespond;
import net.gagarinn.finalproject.model.explore.Group;
import net.gagarinn.finalproject.model.explore.Item;
import net.gagarinn.finalproject.model.search.SearchResponse;
import net.gagarinn.finalproject.model.search.VenuesItem;
import net.gagarinn.finalproject.tools.Tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Burmaka V on 23.01.2018.
 */

public class RetrofitController {

    private static Retrofit retrofit;
    private static FoursquareApi foursquareApi;
    private static DBHelper dbHelper;

    private static double lastLong;
    private static double lastLat;
    private static int lastRadius;


    private  static List<Restorant> lastVenues ;

    public static void initialize(Context context) {
        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.FOURSQUARE_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        foursquareApi = retrofit.create(FoursquareApi.class);
        lastVenues = Collections.<Restorant>emptyList();
        dbHelper = DBHelper.getInstance(context);
    }

    public static void getRestorants(final double lat, final double lon, int radiusMeters, final OnDataRecivedListener listener) {

        if (listener == null) {
            return;
        }

        if (!Tools.isNullOrEmpty(lastVenues) && lastLat == lat && lastLong == lon && lastRadius == radiusMeters){
            listener.onPointsRecived(lastVenues);
            return;
        }

        int radius = radiusMeters;
        if (radius > Constants.MAX_RADIUS_METERS) {
            radius = Constants.MAX_RADIUS_METERS;
        }

        Call<SearchResponse> call = foursquareApi.getRestorantsBySearch("" + lat + "," + lon,"" + radius );
        Log.d("TODEL", "------RetrofitController : run: " + call.request().url());

        final int finalRadius = radius;
        call.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                SearchResponse categoryRespond = response.body();
                if (categoryRespond != null) {
                    if (categoryRespond.getResponse() != null) {
                        setLastRequestedSearchData(categoryRespond.getResponse().getVenues(), lat, lon, finalRadius);
                        listener.onPointsRecived(lastVenues);
                        updateDB(lastVenues);
                        return;
                    }
                }
                listener.onPointsRecived(Collections.<Restorant>emptyList());
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                listener.onPointsRecived(Collections.<Restorant>emptyList());
            }
        });
    }

    public static void getRestorantsSorted(final double lat, final double lon, int radiusMeters, final OnDataRecivedListener listener) {

        if (listener == null) {
            return;
        }

        if (!Tools.isNullOrEmpty(lastVenues) && lastLat == lat && lastLong == lon && lastRadius == radiusMeters){
            listener.onPointsRecived(lastVenues);
            return;
        }

        int radius = radiusMeters;
        if (radius > Constants.MAX_RADIUS_METERS) {
            radius = Constants.MAX_RADIUS_METERS;
        }

        Call<FourSquareRespond> call = foursquareApi.getRestorantsByExplore("" + lat + "," + lon,"" + radius );
        Log.d("TODEL", "------RetrofitController : run: " + call.request().url());

        final int finalRadius = radius;
        call.enqueue(new Callback<FourSquareRespond>() {
            @Override
            public void onResponse(Call<FourSquareRespond> call, Response<FourSquareRespond> response) {
                FourSquareRespond categoryRespond = response.body();
                if (categoryRespond != null) {
                    if (categoryRespond.getResponse() != null) {
                        setLastRequestedExploreData(categoryRespond.getResponse().getGroups(), lat, lon, finalRadius);
                        listener.onPointsRecived(lastVenues);
                        updateDB(lastVenues);
                        return;
                    }
                }
                listener.onPointsRecived(Collections.<Restorant>emptyList());
            }

            @Override
            public void onFailure(Call<FourSquareRespond> call, Throwable t) {
                listener.onPointsRecived(Collections.<Restorant>emptyList());
            }
        });
    }

    private static void updateDB(List<Restorant> venues) {
        dbHelper.saveRestorants(venues);
    }

    private static void setLastRequestedExploreData(List<Group> groups, double lat, double lon, int radius) {
        lastLat = lat;
        lastLong = lon;
        lastRadius = radius;
        if (groups != null){
            lastVenues = new ArrayList<Restorant>();
            for (Group group: groups) {
                    if (group != null){
                        List<Item> items = group.getItems();
                        if (items != null){
                            for (Item item: items) {
                                if (item != null) {
                                    Restorant rest = new Restorant(item);
                                    lastVenues.add(rest);
                                }
                            }
                        }
                    }

            }
        }
    }

    private static void setLastRequestedSearchData(List<VenuesItem> venues, double lat, double lon, int radius) {
        lastLat = lat;
        lastLong = lon;
        lastRadius = radius;
        if (venues != null){
            lastVenues = new ArrayList<Restorant>();
            for (VenuesItem venuesItem: venues) {
                Restorant rest = new Restorant(venuesItem);
                lastVenues.add(rest);
            }
            Collections.sort(lastVenues, new Comparator<Restorant>() {
                @Override
                public int compare(Restorant restorant1, Restorant restorant2) {
                    if (restorant1.getDistance() > restorant2.getDistance())
                        return 1;
                    if (restorant1.getDistance() < restorant2.getDistance())
                        return -1;
                    return 0;
                }
            });
        }
    }
    public static List<Restorant> getLastVenues() {
        if (Tools.isNullOrEmpty(lastVenues)){
            lastVenues = dbHelper.getSavedRestorants();
        }
        return lastVenues;
    }

    public interface OnDataRecivedListener {
        void onPointsRecived(List<Restorant> venues);
    }

    //    public static FourSquareRespond getTestRespond2(){
    // TODO: 26.01.2018 make as test
//        if (instance == null){
//            initialize();
//        }
//        final FourSquareRespond[] fourSquareRespond = {null};
//
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        fourSquareRespond[0] = getApi().getTestData2(Constants.FOURSQUARE_CLIENT_ID,
//                                Constants.FOURSQUARE_CLIENT_SECRET,
//                                "20170801",
//                                "40.7243,-74.0018",
//                                "coffee",
//                                "1"
//                                ).execute().body();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    Log.w("TODEL", "------ RetrofitController.getTestRespond2(): fourSquareRespond = " + fourSquareRespond[0]);
//                }
//            }).start();
//
//
//        return fourSquareRespond[0];
//    }
}
