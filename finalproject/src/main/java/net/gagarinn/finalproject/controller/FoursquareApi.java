package net.gagarinn.finalproject.controller;

import net.gagarinn.finalproject.model.Constants;
import net.gagarinn.finalproject.model.explore.FourSquareRespond;
import net.gagarinn.finalproject.model.search.SearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Burmaka V on 23.01.2018.
 */

public interface FoursquareApi {


    @GET("search/?client_id=" + Constants.FOURSQUARE_CLIENT_ID +"&client_secret=" +Constants.FOURSQUARE_CLIENT_SECRET+"&v=" + Constants.FOURSQUARE_VERSION + "&categoryId=" + Constants.FOURSQUARE_ALL_FOOD_CATEGORY_ID + "&limit=100")
    Call<SearchResponse> getRestorantsBySearch(@Query("ll") String ll, @Query("radius") String radius );

    @GET("explore/?client_id=" + Constants.FOURSQUARE_CLIENT_ID +"&client_secret=" +Constants.FOURSQUARE_CLIENT_SECRET+"&v=" + Constants.FOURSQUARE_VERSION + "&categoryId=" + Constants.FOURSQUARE_ALL_FOOD_CATEGORY_ID + "&limit=100" + "&sortByDistance=1")
    Call<FourSquareRespond> getRestorantsByExplore(@Query("ll") String ll, @Query("radius") String radius );

// TODO: 31.01.2018 test
//    @GET("explore")
//    Call<FourSquareRespond> getTestData2(@Query("client_id") String clientID,
//                                        @Query("client_secret") String clientSecret,
//                                        @Query("v") String v,
//                                        @Query("ll") String ll,
//                                        @Query("query") String query,
//                                        @Query("limit") String limit
//    );
}
