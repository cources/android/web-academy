package net.gagarinn.finalproject.tools;

import java.util.List;

/**
 * Created by Android on 26.01.2018.
 */

public class Tools {
    public static <T> boolean isNullOrEmpty(List<T> items){
        return items == null || items.size() == 0;
    }
}
