package net.gagarinn.finalproject.tools;

import android.app.Application;

import net.gagarinn.finalproject.controller.RetrofitController;

/**
 * Created by Android on 26.01.2018.
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        RetrofitController.initialize(this);
    }
}
