package net.gagarinn.finalproject.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import net.gagarinn.finalproject.R;
import net.gagarinn.finalproject.model.Constants;
import net.gagarinn.finalproject.model.Restorant;

public class ResorantDetailsActivity extends AppCompatActivity {

    TextView tvRestorantName;
    TextView tvDistance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resorant_details);

        Intent intent = getIntent();
        if (intent == null){
            return;
        }

        Restorant restorant = (Restorant)intent.getSerializableExtra(Constants.SELECTED_RESTORANT);
        if (restorant == null){
            return;
        }
        tvRestorantName = (TextView)findViewById(R.id.tvRestorantName);
        tvDistance = (TextView)findViewById(R.id.tvRestorantDistance);

        tvRestorantName.setText(restorant.getName());
        tvDistance.setText(restorant.getDistance() + " m ");

    }
}
