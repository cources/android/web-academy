package net.gagarinn.finalproject.model;

/**
 * Created by Burmaka V on 25.01.2018.
 */

public class Constants {
    public static final String FOURSQUARE_BASE_URL = "https://api.foursquare.com/v2/venues/";
    public static final String FOURSQUARE_CLIENT_ID = "AFQBWU1JIRTO03GSTEOT1B3CMV4HR3XXFZT5W4RLZ2KKMOHB";
    public static final String FOURSQUARE_CLIENT_SECRET = "W3DVGYWMAV3ZWI120QW3O4BOF2GAPTI5X44TGZ00VBH1FUVC";
    public static final String FOURSQUARE_ALL_FOOD_CATEGORY_ID = "4d4b7105d754a06374d81259";
    public static final String FOURSQUARE_VERSION = "20170801";


    public static final String FOURSQUARE_KEY_CLIENT_ID = "client_id";
    public static final String FOURSQUARE_KEY_CLIENT_SECRET = "client_secret";
    public static final String FOURSQUARE_KEY_V = "v";
    public static final String FOURSQUARE_KEY_LL = "ll";
    public static final String FOURSQUARE_KEY_QUERY = "query";
    public static final String FOURSQUARE_KEY_LIMIT = "limit";
    public static final String FOURSQUARE_KEY_RADIUS = "radius";
    public static final String FOURSQUARE_KEY_CATEGORYID = "categoryId";

    public static final int MAX_RADIUS_METERS = 25000;
    public static final int MIN_RADIUS_METERS = 500;
    public static final int MAX_ZOOM = 17;
    public static final int MIN_ZOOM = 10;

    public static final String SELECTED_RESTORANT = "SELECTED_RESTORANT";
}
