package net.gagarinn.finalproject.model.search;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Response{

	@SerializedName("confident")
	private boolean confident;

	@SerializedName("venues")
	private List<VenuesItem> venues;

	public void setConfident(boolean confident){
		this.confident = confident;
	}

	public boolean isConfident(){
		return confident;
	}

	public void setVenues(List<VenuesItem> venues){
		this.venues = venues;
	}

	public List<VenuesItem> getVenues(){
		return venues;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"confident = '" + confident + '\'' + 
			",venues = '" + venues + '\'' + 
			"}";
		}
}