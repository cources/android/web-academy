package net.gagarinn.finalproject.model;

import net.gagarinn.finalproject.model.explore.Item;
import net.gagarinn.finalproject.model.explore.Location;
import net.gagarinn.finalproject.model.explore.Price;
import net.gagarinn.finalproject.model.explore.Stats;
import net.gagarinn.finalproject.model.search.VenuesItem;

import java.io.Serializable;

/**
 * Created by Android on 30.01.2018.
 */

public class Restorant implements Serializable{
    private String id;
    private String name;
    //    location
    private double lng;
    private double lat;
    private long distance;
    //    stats
    private long checkinsCount;
    private long usersCount;
    private long tipCount;

    private long price;
    private double rating;


    public Restorant() {
    }

    public Restorant(VenuesItem venuesItem) {
        this.id = venuesItem.getId();
        this.name = venuesItem.getName();
        if (venuesItem.getLocation() != null) {
            this.lat = venuesItem.getLocation().getLat();
            this.lng = venuesItem.getLocation().getLng();
            this.distance = venuesItem.getLocation().getDistance();
        }
        if (venuesItem.getStats() != null) {
            this.checkinsCount = venuesItem.getStats().getCheckinsCount();
            this.usersCount = venuesItem.getStats().getUsersCount();
            this.tipCount = venuesItem.getStats().getTipCount();
        }
    }

    public Restorant(String id, String name, double lng, double lat, int distance, int checkinsCount, int usersCount, int tipCount) {

    }

    public Restorant(Item item) {
        this.id = item.getId();
        if (item.getVenue() != null) {
            this.name = item.getVenue().getName();
            this.rating = item.getVenue().getRating();

            Location location = item.getVenue().getLocation();
            if (location != null) {
                this.lng = location.getLng();
                this.lat = location.getLat();
                this.distance = location.getDistance();
            }
            Stats stats = item.getVenue().getStats();
            if (stats != null) {
                this.checkinsCount = stats.getCheckinsCount();
                this.usersCount = stats.getUsersCount();
                this.tipCount = stats.getTipCount();
            }
            Price price = item.getVenue().getPrice();
            if (price != null){
                this.price = price.getTier();
            }
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public long getDistance() {
        return distance;
    }

    public void setDistance(long distance) {
        this.distance = distance;
    }

    public long getCheckinsCount() {
        return checkinsCount;
    }

    public void setCheckinsCount(long checkinsCount) {
        this.checkinsCount = checkinsCount;
    }

    public long getUsersCount() {
        return usersCount;
    }

    public void setUsersCount(long usersCount) {
        this.usersCount = usersCount;
    }

    public long getTipCount() {
        return tipCount;
    }

    public void setTipCount(long tipCount) {
        this.tipCount = tipCount;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}