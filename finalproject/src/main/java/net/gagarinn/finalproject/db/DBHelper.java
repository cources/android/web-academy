package net.gagarinn.finalproject.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import net.gagarinn.finalproject.model.Restorant;
import net.gagarinn.finalproject.model.search.VenuesItem;
import net.gagarinn.finalproject.tools.Tools;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Android on 30.01.2018.
 */

public class DBHelper extends SQLiteOpenHelper implements IDBContract {

    private static String TAG = "DBHelper";
    private static DBHelper instance;

    private static final String DB_NAME = "main_app_db";
    private static final int DB_VERSION = 1;

    private static final String ID = "_id";

    private static final String RESTORANTS_TABLE_NAME = "venues_table_name";
    private static final String RESTORANT_ID = "id";
    private static final String NAME = "name";
    //    location
    private static final String LNG = "lng";
    private static final String LAT = "lat";
    private static final String DISTANCE = "distance";

    private static final String PRICE = "price";
    private static final String RATING = "rating";
    //    stats
    private static final String CHECKINSCOUNT = "checkinsCount";
    private static final String TIPCOUNT = "tipCount";
    private static final String USERSCOUNT = "usersCount";

//            4. В режиме списка должна отображаться следующая информация: название ресторана и расстояние до указанного пользователем адреса или его места положением. Так же, рестораны должны быть отсортированы в списке по дистанции до выбранного пользователем адреса или локации.
//5.Как в режиме карты так и по нажатию на элемент списка должен открываться экран (или показываться диалог) в котором будет выведены имя ресторана, контактный номер телефона, адрес и ценовой рейтинг ($, $$  и т.п.)


    public static synchronized DBHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DBHelper(context, DB_NAME, null, DB_VERSION);
        }
//        logTables();
        return instance;
    }



    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    private static String createRestorauntsTable() {
        return "CREATE TABLE IF NOT EXISTS " + RESTORANTS_TABLE_NAME + " (" +
                ID + " INTEGER  PRIMARY KEY AUTOINCREMENT," +
                RESTORANT_ID + " TEXT," +
                NAME + " TEXT, " +
                LNG + " DOUBLE, " +
                LAT + " DOUBLE, " +
                DISTANCE + " BIGINT, " +
                CHECKINSCOUNT + " BIGINT, " +
                TIPCOUNT + " BIGINT, " +
                USERSCOUNT + " BIGINT, " +
                PRICE + " BIGINT, " +
                RATING + " DOUBLE" +
                ")";
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            sqLiteDatabase.execSQL(createRestorauntsTable());
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

    private static void logTables() {
        SQLiteDatabase db = instance.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);

        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                Log.e("TODEL", "------ DBHelper.onCreate: = " + c.getString( c.getColumnIndex("name")));
//                arrTblNames.add( c.getString( c.getColumnIndex("name")) );
                c.moveToNext();
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // TODO: 30.01.2018
    }

    @Override
    public void saveRestorants(List<Restorant> venues) {
        if (Tools.isNullOrEmpty(venues)) {
            return;
        }
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        try {
            db.execSQL("DELETE FROM " + RESTORANTS_TABLE_NAME);
            for (Restorant restorant : venues) {
                cv.put(RESTORANT_ID, restorant.getId());
                cv.put(NAME, restorant.getName());
                cv.put(LNG, restorant.getLng());
                cv.put(LAT, restorant.getLat());
                cv.put(DISTANCE, restorant.getDistance());
                cv.put(CHECKINSCOUNT, restorant.getCheckinsCount());
                cv.put(TIPCOUNT, restorant.getTipCount());
                cv.put(USERSCOUNT, restorant.getUsersCount());
                cv.put(PRICE, restorant.getPrice());
                cv.put(RATING, restorant.getRating());

                long result = db.insert(RESTORANTS_TABLE_NAME, null, cv);
            }
        } catch (Exception e) {
            Log.e(TAG, "------MessageDB : insertMessage " + e.getMessage());
        } finally {
            db.close();
        }
    }

    @Override
    public List<Restorant> getSavedRestorants() {
        List<Restorant> venues = new ArrayList<Restorant>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = null;

        try {
            cursor = db.rawQuery("SELECT * FROM " + RESTORANTS_TABLE_NAME, null);
            if (cursor == null) {
                return venues;
            }
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
            db.close();
            return venues;
        }

        if (cursor.moveToFirst()) {
            int idColIndex = cursor.getColumnIndex(ID);
            int restIdColIndex = cursor.getColumnIndex(RESTORANT_ID);
            int nameColIndex = cursor.getColumnIndex(NAME);
            int lngColIndex = cursor.getColumnIndex(LNG);
            int latColIndex = cursor.getColumnIndex(LAT);
            int distanceColIndex = cursor.getColumnIndex(DISTANCE);
            int checkColIndex = cursor.getColumnIndex(CHECKINSCOUNT);
            int tipColIndex = cursor.getColumnIndex(TIPCOUNT);
            int usersColIndex = cursor.getColumnIndex(USERSCOUNT);
            int priceColIndex = cursor.getColumnIndex(PRICE);
            int ratingColIndex = cursor.getColumnIndex(RATING);
            do {
                Restorant restorant = new Restorant();

                restorant.setId(cursor.getString(restIdColIndex));
                restorant.setName(cursor.getString(nameColIndex));
                restorant.setLng(cursor.getDouble(lngColIndex));
                restorant.setLat(cursor.getDouble(latColIndex));
                restorant.setDistance(cursor.getInt(distanceColIndex));
                restorant.setCheckinsCount(cursor.getInt(checkColIndex));
                restorant.setTipCount(cursor.getInt(tipColIndex));
                restorant.setUsersCount(cursor.getInt(usersColIndex));
                restorant.setPrice(cursor.getLong(priceColIndex));
                restorant.setRating(cursor.getDouble(ratingColIndex));
                venues.add(restorant);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        Log.e(TAG, "------DBHelper : getSavedRestorants: " + venues.size());
        return venues;
    }
}
