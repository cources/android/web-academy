package net.gagarinn.finalproject.db;

import net.gagarinn.finalproject.model.Restorant;
import net.gagarinn.finalproject.model.search.VenuesItem;

import java.util.List;

/**
 * Created by Android on 30.01.2018.
 */

public interface IDBContract {
    void saveRestorants(List<Restorant> venues);
    List<Restorant> getSavedRestorants();
}
