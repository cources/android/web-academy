package net.gagarinn.lesson01;

/**
 * Created by Burmaka V on 12.12.2017.
 */

public class Constants {
    public static final int KEY_RESULT_BUNDLE = -1;
    public static int NEXT = 0;
    public static int PREVIOS = 1;
    public static int START_SLIDE_SHOW = 2;
    public static int STOP_SLIDE_SHOW = 3;
    public static int STOP_THE_SERVICE = 4;
    public static String KEY_ACTION = "KEY_ACTION";
    public static String KEY_RESULT_RECEIVER = "KEY_RESULT_RECEIVER";
    public static String KEY_HAS_NEXT = "KEY_HAS_NEXT";
    public static String KEY_HAS_PREVIOS = "KEY_HAS_PREVIOS";



}
