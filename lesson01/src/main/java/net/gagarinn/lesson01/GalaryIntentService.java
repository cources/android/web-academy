package net.gagarinn.lesson01;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;
import android.util.Log;


public class GalaryIntentService extends IntentService {
    private final String TAG = getClass().getSimpleName();

    private ResultReceiver activityResultReceiver;
    private ResultReceiver serviceResultReceiver;

    public GalaryIntentService() {
        super(null); // That was the lacking constructor
    }
    public GalaryIntentService(String name) {
        super(name);
    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e(TAG, "------GalaryIntentService : onHandleIntent: hashCode = " + hashCode());
        Log.e(TAG, "------GalaryIntentService : onHandleIntent: hashCode = " + Thread.currentThread().getId());
        if (serviceResultReceiver == null) {
            serviceResultReceiver = new ResultReceiver(new Handler()) {

                @Override
                protected void onReceiveResult(int resultCode, Bundle resultData) {
                    int action = -5;
                    if (resultData != null){
                        action =  resultData.getInt(Constants.KEY_ACTION);

                        Bundle bundle = new Bundle();
                        bundle.putInt(Constants.KEY_ACTION, action);
                        activityResultReceiver.send(Constants.KEY_RESULT_BUNDLE, bundle);
                    }
                    Log.e(TAG, "------GalaryIntentService : onReceiveResult: resultCode = " + resultCode);
                    Log.e(TAG, "------GalaryIntentService : onReceiveResult: action = " + action);
                }

            };
        }

        if (intent != null) {
            activityResultReceiver = intent.getParcelableExtra(Constants.KEY_RESULT_RECEIVER);
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.KEY_RESULT_RECEIVER, serviceResultReceiver);
            activityResultReceiver.send(Constants.KEY_RESULT_BUNDLE, bundle);
        }
    }


}
