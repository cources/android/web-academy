package net.gagarinn.lesson01;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final String TAG = getClass().getSimpleName();

    private ResultReceiver activityResultReceiver;
    private ResultReceiver serviceResultReceiver;
    private Intent intent;


    private ImageButton ibtnNext;
    private ImageButton ibtnPrev;
    private ImageButton ibtnSlideShow;
    private ImageView ivMainImage;
    private TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.e(TAG, "------MainActivity : onCreate: hashCode = " + Thread.currentThread().getId());
        if (intent == null) {
            intent = new Intent(this, GalaryIntentService.class);
        }

        ibtnNext = (ImageButton) findViewById(R.id.ibtn_next);
        ibtnPrev = (ImageButton) findViewById(R.id.ibtn_prev);
        ibtnSlideShow = (ImageButton) findViewById(R.id.ibtn_slide);
        ivMainImage = (ImageView) findViewById(R.id.iv_main_image);
        tvTitle = (TextView) findViewById(R.id.tv_title);

        ibtnNext.setOnClickListener(this);
        ibtnPrev.setOnClickListener(this);
        ibtnSlideShow.setOnClickListener(this);

        activityResultReceiver = new ResultReceiver(new Handler()) {

            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData == null){return;}

                int resultFromService = -5;
                if (serviceResultReceiver == null){
                    serviceResultReceiver = resultData.getParcelable(Constants.KEY_RESULT_RECEIVER);
                }
                resultFromService = resultData.getInt(Constants.KEY_ACTION);
                Log.e(TAG, "------MainActivity : onReceiveResult: resultCode = " + resultCode);
                Log.e(TAG, "------MainActivity : onReceiveResult: resultFromService = " + resultFromService);
            }

        };
        intent.putExtra(Constants.KEY_RESULT_RECEIVER, activityResultReceiver);
        startService(intent);

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onClick(View view) {
        if (serviceResultReceiver == null){
            return;
        }

        int viewId = view.getId();
        switch (viewId) {
            case R.id.ibtn_next: {
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.KEY_ACTION, Constants.NEXT);
                serviceResultReceiver.send(Constants.KEY_RESULT_BUNDLE, bundle);

//                intent.putExtra(Constants.KEY_RESULT_RECEIVER, activityResultReceiver);
//                intent.putExtra(Constants.KEY_ACTION, Constants.NEXT);
//                startService(intent);
                break;
            }
            case R.id.ibtn_prev: {
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.KEY_ACTION, Constants.PREVIOS);
                serviceResultReceiver.send(Constants.KEY_RESULT_BUNDLE, bundle);
//                intent.putExtra(Constants.KEY_RESULT_RECEIVER, activityResultReceiver);
//                intent.putExtra(Constants.KEY_ACTION, Constants.PREVIOS);
//                startService(intent);
                break;
            }
            case R.id.ibtn_slide: {
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.KEY_ACTION, Constants.START_SLIDE_SHOW);
                serviceResultReceiver.send(Constants.KEY_RESULT_BUNDLE, bundle);
//                intent.putExtra(Constants.KEY_RESULT_RECEIVER, activityResultReceiver);
//                intent.putExtra(Constants.KEY_ACTION, Constants.START_SLIDE_SHOW);
//                startService(intent);
                break;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopService(intent);
    }
}
